from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QHBoxLayout, QVBoxLayout, QGridLayout, QLabel, QSpinBox
from accwidgets.lsa_selector import AbstractLsaSelectorContext, LsaSelector, LsaSelectorModel, LsaSelectorAccelerator
from matplotlib.backends.backend_qtagg import FigureCanvasQTAgg
from matplotlib.figure import Figure
from pyjapc import PyJapc
import pjlsa
import sys

class MyWidget(QWidget):

    def __init__(self):
        super().__init__()
        self.lsa = pjlsa.LSAClient("PS")
        self.setGeometry(1000, 200, 1200, 550)
        self.lsa_selector = LsaSelector(parent=self,
                                        model=LsaSelectorModel(LsaSelectorAccelerator.PS, self.lsa, resident_only=False))
        self.lsa_selector.contextSelectionChanged.connect(self.lsa_selected)

        self.sc = MplCanvas(self, width=7, height=5, dpi=100)
        self.japc = PyJapc(noSet=True, selector='CPS.USER.ZERO')
        to_print = self.japc.getParam('PR.SCOPE58.CH01/Acquisition')

        main_layout = QHBoxLayout()
        main_layout.addWidget(self.lsa_selector)
        main_layout.addWidget(self.sc)

        self.setLayout(main_layout)

        self.sc.axes.imshow(to_print['value'])
        #self.sc.axes.clear()                       #    it works

    def lsa_selected(self, ctx: AbstractLsaSelectorContext):

        print('function works ' + ctx.user)
        self.sc.axes.clear()                        # IT DOES NOT WORK

class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)


app = QApplication([])
window = MyWidget()
window.show()
sys.exit(app.exec())
